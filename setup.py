import gdown
import os
from zipfile import ZipFile

FULL_DATASET = False

if FULL_DATASET:
   url = 'https://drive.google.com/uc?id=1gI7fwpU-X2oo8jtqXbtPEJO34u0Uz_7z'
   md5 = '20f308dc40e17fe68db48d26667bd7ca'
else:
   url = 'https://drive.google.com/u/1/uc?id=1XGN0HQKar-ApXrn0XljYQxTnbIP5sjfz&export=download'
   md5 = '84a53c17a9e73f2fce57e7d8d5abd946'

output = 'dataset.zip'
gdown.cached_download(url, output, md5=md5, postprocess=gdown.extractall)

with ZipFile(output, 'r') as zipObj:
   # Extract all the contents of zip file in current directory
   zipObj.extractall()
