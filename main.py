import argparse

from src.file_support import get_model_names
from common import TEST_ITERATIONS, USE_TPU
from src import Benchmark


if __name__ == '__main__':

    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--iterations", help="Number of performed tests", default=TEST_ITERATIONS)

    args = vars(ap.parse_args())

    for model_name in get_model_names():
        if (USE_TPU and ('edgetpu' in model_name)) or (not USE_TPU and (not'edgetpu' in model_name)):
            benchmark = Benchmark(model_name)
            benchmark.run_test(int(args['iterations']))
